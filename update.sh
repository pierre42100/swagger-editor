#!/bin/bash
URL=$(curl https://registry.npmjs.org/swagger-editor-dist | jq . | grep tar | tail -n 1 | cut -d'"' -f 4)
wget -4 "$URL"
tar -xf *.tgz
rm *.tgz
mv package/* .
rmdir package
echo "Done"
